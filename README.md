# Salesleadership Academy [![Developed by](https://img.shields.io/badge/Developed%20by-Nooijen%20Web%20Solutions-green.svg)](https://nooijensolutions.nl) [![GPL Licence](https://badges.frapsoft.com/os/gpl/gpl.svg?v=103)](https://opensource.org/licenses/GPL-3.0/) [![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

[![Build Status](https://travis-ci.org/BytecodeBV/Salesleadership-Academy.svg?branch=master)](https://travis-ci.org/BytecodeBV/Salesleadership-Academy)
[![dependencies Status](https://david-dm.org/BytecodeBV/Salesleadership-Academy/status.svg)](https://david-dm.org/BytecodeBV/Salesleadership-Academy)
[![devDependencies Status](https://david-dm.org/BytecodeBV/Salesleadership-Academy/dev-status.svg)](https://david-dm.org/BytecodeBV/Salesleadership-Academy?type=dev)
[![Greenkeeper badge](https://badges.greenkeeper.io/BytecodeBV/Salesleadership-Academy.svg)](https://greenkeeper.io/)
[![CodeFactor](https://www.codefactor.io/repository/github/BytecodeBV/Salesleadership-academy/badge)](https://www.codefactor.io/repository/github/BytecodeBV/Salesleadership-academy)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/3c22a93c01ec4efc841f3387beeee68c)](https://www.codacy.com/app/lucianonooijen/Salesleadership-Academy?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=BytecodeBV/Salesleadership-Academy&amp;utm_campaign=Badge_Grade)
[![BCH compliance](https://bettercodehub.com/edge/badge/BytecodeBV/Salesleadership-Academy?branch=master)](https://bettercodehub.com/)
[![bitHound Code](https://www.bithound.io/github/BytecodeBV/Salesleadership-Academy/badges/code.svg)](https://www.bithound.io/github/BytecodeBV/Salesleadership-Academy)
[![Average time to resolve an issue](http://isitmaintained.com/badge/resolution/BytecodeBV/Salesleadership-academy.svg)](http://isitmaintained.com/project/BytecodeBV/Salesleadership-academy "Average time to resolve an issue")
[![Percentage of issues still open](http://isitmaintained.com/badge/open/BytecodeBV/Salesleadership-academy.svg)](http://isitmaintained.com/project/BytecodeBV/Salesleadership-academy "Percentage of issues still open")
[![Known Vulnerabilities](https://snyk.io/test/github/BytecodeBV/Salesleadership-academy/badge.svg?targetFile=package.json)](https://snyk.io/test/github/BytecodeBV/Salesleadership-academy?targetFile=package.json)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![codecov](https://codecov.io/gh/BytecodeBV/Salesleadership-Academy/branch/master/graph/badge.svg)](https://codecov.io/gh/BytecodeBV/Salesleadership-Academy)

## Development instructions

To run the development environment, you will need NodeJS and Yarn installed on your machine.

First, you will need to install the dependencies using:

```sh
yarn
```

To run the local development server, run the following command:

```sh
yarn run dev
```

The development server will now be available on `localhost:1234`. To run tests for your code, you can run the `jest` command. For creating the docs for the Javascript code, run `yarn run esdoc`

## Production build instructions

To create the production build, run

```sh
yarn run build
```

This command will build to the dist folder (static html to show the blocks) and to the plate/assets folder, so that the built assets will be used on the Plate website. To run a mock production server using the static html files, run

```sh
yarn run start
```

This will start a Express server you can view on `localhost:3000`. You can also run `nodemon` if installed globally.

## Uploading to Plate

To upload the theme to plate, you will need a Plate API key. Using this API key you can use [Plate Theme Tool](https://platehub.github.io/docs/themetool/).

You can check the Plate developer documentation on [platehub.github.io/docs](https://platehub.github.io/docs/).

## Issues

For issues, you can open an issue here on Github, or email to [support@bytecode.nl](mailto:support@bytecode.nl)

## Contributors @ Bytecode Digital Agency B.V.

* Luciano Nooijen
* Jeroen van Steijn
* Richard van 't Hof

### License

GPL-3.0
